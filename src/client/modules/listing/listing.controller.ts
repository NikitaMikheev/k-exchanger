import { Controller, Post, Req, Res, Body, Get } from '@nestjs/common';
import { Response } from 'express';
import { UserRequest } from 'src/general/interfaces/controller.interface';
import { ListingService } from './listing.service';
import { AddNewApplicationToListingDto } from '../dto/add-application-to-listing.dto';
import { GetListListingFiltersDto } from '../dto/get-listing-list.dto';

@Controller('/listing')
export class ListingController {
  constructor(private listingService: ListingService) {}

  @Get('/list')
  async getListingList(@Body() filters: GetListListingFiltersDto, @Req() req: UserRequest, res: Response) {
    const listingList = await this.listingService.getList(filters);
    res.status(200).json(listingList);
  }

  @Post('/add')
  async addNewApplicationToListing(
    @Body() dataForNewApplicationForListing: AddNewApplicationToListingDto,
    @Req() req: UserRequest,
    @Res() res: Response
  ) {
    const newPositionForSellingOrExchange = await this.listingService.createNewApplicationForListing(
      dataForNewApplicationForListing,
      req.user
    );
    res.status(201).json(newPositionForSellingOrExchange);
  }
}
