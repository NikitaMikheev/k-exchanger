import { ArgumentsHost, Catch, ExceptionFilter, HttpException, Logger } from '@nestjs/common';

import { Request, Response } from 'express';

@Catch()
export class AllExceptionFilter implements ExceptionFilter {
  private readonly logger: Logger = new Logger(AllExceptionFilter.name);

  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    if (exception instanceof HttpException) {
      const status: number = exception.getStatus();
      const body = exception.getResponse();
      response.status(status).json(body);
    } else {
      const timestamp = new Date();
      const errorForLog = {
        timestamp,
        method: request.method,
        url: request.url,
        exception: (exception as Error)?.stack || String(exception)
      };
      this.logger.error('Unhandled exception', errorForLog);
      response.status(500).json({ status: 'Internal server error' });
    }
  }
}
