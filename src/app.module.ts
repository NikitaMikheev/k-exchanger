import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './database/database.module';
import { ConfigModule } from '@nestjs/config';
import { ClientModule } from './client/client.module';
import { AdminModule } from './admin/admin.module';
import { RouterModule } from '@nestjs/core';
import { JwtMiddleware } from './middleware/verifyJwt';
import { SequelizeModule } from '@nestjs/sequelize';
import { UserModel } from './database/models/user.model';

@Module({
  imports: [
    AuthModule,
    ClientModule,
    AdminModule,
    DatabaseModule,
    SequelizeModule.forFeature([UserModel]),
    ConfigModule.forRoot({ isGlobal: true }),
    RouterModule.register([
      { path: 'client', module: ClientModule },
      { path: 'admin', module: AdminModule }
    ])
  ]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(JwtMiddleware)
      .exclude(
        { path: 'auth/registration', method: RequestMethod.ALL },
        { path: 'auth/login', method: RequestMethod.ALL }
      )
      .forRoutes('*');
  }
}
