export class GetListListingFiltersDto {
  page?: number;
  limit?: number;
  category: 'album' | 'card' | 'other';
  user_id?: string;
  item_types: string[];
  listing_type: 'sale' | 'exchange' | 'all';
  order?: [[string, 'DESC' | 'ASC']];
}
