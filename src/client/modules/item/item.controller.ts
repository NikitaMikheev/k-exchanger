import { Body, Controller, Post, Get, Req, Res } from '@nestjs/common';
import { Response } from 'express';
import { ItemService } from './item.service';
import { CreateItemDto } from '../dto/create-item.dto';
import { UserRequest } from 'src/general/interfaces/controller.interface';

@Controller('item')
export class ItemController {
  constructor(private itemService: ItemService) {}

  @Get('/list')
  async getItemList(@Req() req: UserRequest, @Res() res: Response) {
    const itemList = await this.itemService.getItemList(req.user);
    res.status(200).json(itemList);
  }

  @Post('/create')
  async createAndAddToInventary(@Body() createItemData: CreateItemDto, @Req() req: UserRequest, @Res() res: Response) {
    const newItem = await this.itemService.addNewItem(createItemData, req.user);
    res.status(201).json(newItem);
  }
}
