import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UserModel } from 'src/database/models/user.model';

@Module({
  imports: [SequelizeModule.forFeature([UserModel])],
  controllers: [AuthController],
  providers: [AuthService]
})
export class AuthModule {}
