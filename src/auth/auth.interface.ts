export interface IDataForNewUser {
  email: string;
  username: string;
  password: string;
  secretToken: string;
}

export interface IUserDataResponse {
  id: string;
  email: string;
  balance: number;
  username: string;
  role: 'client' | 'admin';
  accessToken: string;
}
