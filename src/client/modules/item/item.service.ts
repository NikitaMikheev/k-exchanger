import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { ItemModel } from 'src/database/models/item.model';
import { CreateItemDto } from '../dto/create-item.dto';
import { UserModel } from 'src/database/models/user.model';

@Injectable()
export class ItemService {
  constructor(@InjectModel(ItemModel) private itemModel: typeof ItemModel) {}

  async getItemList(user: UserModel): Promise<ItemModel[]> {
    const itemList = await this.itemModel.findAll({ where: { user_id: user.id } });
    return itemList;
  }

  async addNewItem(createItemData: CreateItemDto, user: UserModel): Promise<ItemModel> {
    const dataForNewItem = { ...createItemData, user_id: user.id };
    const newItem = await this.itemModel.create(dataForNewItem);
    return newItem;
  }
}
