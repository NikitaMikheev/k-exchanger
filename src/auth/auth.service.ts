import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { UserModel } from 'src/database/models/user.model';
import { CreateUserDto } from './dto/create-user.dto';
import { AuthUserDto } from './dto/auth-user.dto';
import { CreationAttributes } from 'sequelize';
import { generateToken } from 'src/tool/generateToken';
import { IUserDataResponse } from './auth.interface';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(@InjectModel(UserModel) private userModel: typeof UserModel) {}

  async registration(userData: CreateUserDto): Promise<{ user: IUserDataResponse; refreshToken: string }> {
    const dataForNewUser: CreationAttributes<UserModel> = {
      username: userData.username,
      password: userData.password,
      email: userData.email
    };
    const user = await this.userModel.create(dataForNewUser, {
      returning: ['id', 'balance', 'username', 'email', 'role', 'refreshToken']
    });

    const dataForResponse = {
      id: user.id,
      balance: user.balance,
      username: user.username,
      email: user.email,
      role: user.role,
      accessToken: generateToken(user, Number(process.env.ACCESS_TOKEN_LIFE_TIME) || 60 * 60 * 1000)
    };
    return { user: dataForResponse, refreshToken: user.refreshToken };
  }

  async login(authData: AuthUserDto): Promise<{ user: IUserDataResponse; refreshToken: string }> {
    const user = await this.userModel.findOne({
      where: { username: authData.username }
    });
    if (!user || !(await bcrypt.compare(authData.password, user.password))) {
      throw new UnauthorizedException('Login or password is incorrect');
    }
    const dataForResponse = {
      id: user.id,
      balance: user.balance,
      username: user.username,
      email: user.email,
      role: user.role,
      accessToken: generateToken(user, Number(process.env.ACCESS_TOKEN_LIFE_TIME) || 60 * 60 * 1000)
    };
    return { user: dataForResponse, refreshToken: user.refreshToken };
  }
}
