import { IsNotEmpty, MinLength } from 'class-validator';

export class AuthUserDto {
  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  @MinLength(8)
  password: string;
}
