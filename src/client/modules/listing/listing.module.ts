import { Module } from '@nestjs/common';
import { ListingController } from './listing.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { ListedItemModel } from 'src/database/models/listed_item.model';
import { ListingService } from './listing.service';

@Module({
  imports: [SequelizeModule.forFeature([ListedItemModel])],
  controllers: [ListingController],
  providers: [ListingService]
})
export class ListingModule {}
