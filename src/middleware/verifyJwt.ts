import { Injectable, NestMiddleware } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/sequelize';
import { Response, NextFunction } from 'express';
import { UserRequest } from 'src/general/interfaces/controller.interface';
import * as jwt from 'jsonwebtoken';
import { UserModel } from 'src/database/models/user.model';

@Injectable()
export class JwtMiddleware implements NestMiddleware {
  constructor(
    private readonly configService: ConfigService,
    @InjectModel(UserModel) private userModel: typeof UserModel
  ) {}

  async use(req: UserRequest, res: Response, next: NextFunction) {
    try {
      const token = req.headers.authorization.split(' ')[1];
      const payload = jwt.verify(token, this.configService.get<string>('SECRET_FOR_JWT')) as { id: string };
      req.user = await this.userModel.findByPk(payload.id);
      next();
    } catch (error) {
      res.status(401).json({ status: 'Unathorized' });
    }
  }
}
