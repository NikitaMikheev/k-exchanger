import { UserModel } from 'src/database/models/user.model';
import * as jwt from 'jsonwebtoken';

export const generateToken = (user: UserModel, expires: number) => {
  const payload = {
    id: user.id,
    role: user.role
  };
  const token = jwt.sign(payload, process.env.SECRET_FOR_JWT, {
    expiresIn: expires
  });
  return token;
};
