import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Response } from 'express';
import { CreateItemDto } from 'src/client/modules/dto/create-item.dto';
import { UserRequest } from 'src/general/interfaces/controller.interface';
import { AlbumType, CardType } from 'src/tool/enumListOfItemType';

@Injectable()
export class IsValidTypeForCategory implements NestMiddleware {
  constructor() {}

  async use(req: UserRequest, res: Response, next: NextFunction) {
    const createItemData: CreateItemDto = { ...req.body };
    try {
      switch (createItemData.category) {
        case 'card':
          if (!Object.values(CardType).includes(createItemData.type as CardType)) {
            throw new Error();
          }
          break;
        case 'album':
          if (!Object.values(AlbumType).includes(createItemData.type as AlbumType)) {
            throw new Error();
          }
          break;
        case 'poster':
          break;
        case 'cd':
          break;
        default:
          throw new Error();
      }
      next();
    } catch (error) {
      res.status(400).json({ error: 'Incorrect type or category' });
    }
  }
}
