import { Table, Column, Model, DataType } from 'sequelize-typescript';
import { v4 } from 'uuid';

@Table
export class ListedItemModel extends Model {
  @Column({
    type: DataType.STRING,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: () => `card_${v4()}`
  })
  id: string;

  @Column({
    type: DataType.ENUM('selling', 'exchange'),
    allowNull: false
  })
  type: 'selling' | 'exchange';

  @Column({
    type: DataType.DOUBLE,
    allowNull: true
  })
  price: number;

  @Column({
    type: DataType.TEXT,
    allowNull: true
  })
  comment: string;

  @Column({
    type: DataType.UUID,
    allowNull: true
  })
  item_id: string;

  @Column({
    type: DataType.UUID,
    allowNull: true
  })
  user_id: string;
}
