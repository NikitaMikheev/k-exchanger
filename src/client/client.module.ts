import { Module } from '@nestjs/common';
import { ItemModule } from './modules/item/item.module';
import { ListingModule } from './modules/listing/listing.module';

@Module({
  imports: [ItemModule, ListingModule]
})
export class ClientModule {}
