import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { ListedItemModel } from 'src/database/models/listed_item.model';
import { UserModel } from 'src/database/models/user.model';
import { AddNewApplicationToListingDto } from '../dto/add-application-to-listing.dto';
import { GetListListingFiltersDto } from '../dto/get-listing-list.dto';

@Injectable()
export class ListingService {
  constructor(
    @InjectModel(ListedItemModel)
    private listedItemModel: typeof ListedItemModel
  ) {}

  async getList(filters: GetListListingFiltersDto) {
    const where = {};
    if (filters.listing_type !== 'all') {
      where['type'] = filters.listing_type;
    }
    if (filters.user_id) {
      where['user_id'] = filters.user_id;
    }
    const search = {
      limit: filters.limit || 10,
      page: filters.page || 1,
      order: filters.order || [['createdAt', 'DESC']],
      where
    };

    const listingList = await this.listedItemModel.findAll(search);
    return listingList;
  }

  async createNewApplicationForListing(
    dataForNewAppllicationToListing: AddNewApplicationToListingDto,
    user: UserModel
  ) {
    const fullDataForNewAppllicationToListing = {
      ...dataForNewAppllicationToListing,
      user_id: user.id
    };
    const newListedItem = await this.listedItemModel.create(fullDataForNewAppllicationToListing);
    return newListedItem;
  }
}
