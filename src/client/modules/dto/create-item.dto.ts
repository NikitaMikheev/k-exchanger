import { IsString, IsArray, IsOptional, IsEnum, IsNotEmpty, IsBoolean } from 'class-validator';

export class CreateItemDto {
  @IsNotEmpty()
  @IsString()
  group: string;

  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  members: string[];

  @IsOptional()
  @IsString()
  country: string;

  @IsOptional()
  @IsString()
  release_date?: string;

  @IsOptional()
  @IsString()
  edition: string;

  @IsNotEmpty()
  @IsString()
  type: string;

  @IsNotEmpty()
  @IsEnum(['card', 'album', 'poster', 'cd'])
  category: 'card' | 'album' | 'poster' | 'cd';

  @IsOptional()
  @IsString()
  note?: string;

  @IsNotEmpty()
  @IsBoolean()
  isDamaged: boolean;
}
