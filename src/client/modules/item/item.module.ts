import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { ItemController } from './item.controller';
import { ItemService } from './item.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { ItemModel } from 'src/database/models/item.model';
import { IsValidTypeForCategory } from 'src/middleware/isValidTypeForCategory';

@Module({
  imports: [SequelizeModule.forFeature([ItemModel])],
  controllers: [ItemController],
  providers: [ItemService]
})
export class ItemModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(IsValidTypeForCategory).forRoutes({ path: 'item/create', method: RequestMethod.POST });
  }
}
