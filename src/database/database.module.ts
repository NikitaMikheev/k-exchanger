import { Global, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { UserModel } from './models/user.model';
import { ConfigService } from '@nestjs/config';
import { ItemModel } from './models/item.model';
import { ListedItemModel } from './models/listed_item.model';

@Global()
@Module({
  imports: [
    SequelizeModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        dialect: 'postgres',
        host: 'localhost',
        port: 5432,
        username: configService.get<string>('POSTGRES_USER'),
        password: configService.get<string>('POSTGRES_PASSWORD'),
        database: configService.get<string>('POSTGRES_DB'),
        autoLoadModels: true,
        synchronize: true,
        models: [UserModel, ItemModel, ListedItemModel]
      })
    })
  ],
  exports: [SequelizeModule]
})
export class DatabaseModule {}
