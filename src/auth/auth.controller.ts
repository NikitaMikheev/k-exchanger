import { Body, Controller, Post, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CreateUserDto } from './dto/create-user.dto';
import { AuthUserDto } from './dto/auth-user.dto';
import { Response } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/registration')
  async registration(@Body() createUserData: CreateUserDto, @Res() res: Response) {
    const { user, refreshToken } = await this.authService.registration(createUserData);
    res.cookie('refreshToken', refreshToken, {
      httpOnly: true,
      secure: true,
      sameSite: 'strict',
      maxAge: Number(process.env.COOKIE_REFRESH_TOKEN_LIFE_TIME) || 30 * 24 * 60 * 60 * 1000
    });
    res.status(201).json(user);
  }

  @Post('/login')
  async login(@Body() authUserData: AuthUserDto, @Res() res: Response) {
    const { user, refreshToken } = await this.authService.login(authUserData);
    res.cookie('refreshToken', refreshToken, {
      httpOnly: true,
      secure: true,
      sameSite: 'strict',
      maxAge: Number(process.env.COOKIE_REFRESH_TOKEN_LIFE_TIME) || 30 * 24 * 60 * 60 * 1000
    });
    res.status(201).json(user);
  }
}
