import { Table, Column, Model, DataType } from 'sequelize-typescript';
import { v4 } from 'uuid';

@Table
export class ItemModel extends Model {
  @Column({
    type: DataType.STRING,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: () => v4()
  })
  id: string;

  @Column({
    type: DataType.STRING,
    allowNull: true
  })
  country: string;

  @Column({
    type: DataType.STRING,
    allowNull: false
  })
  group: string;

  @Column({
    type: DataType.JSON(),
    allowNull: false
  })
  members: string[];

  @Column({
    type: DataType.STRING,
    allowNull: true
  })
  release_date: string;

  @Column({
    type: DataType.ENUM('card', 'album', 'poster', 'cd', 'other'),
    allowNull: false
  })
  category: 'card' | 'album' | 'poster' | 'cd' | 'other';

  @Column({
    type: DataType.STRING,
    allowNull: false
  })
  type: string;

  @Column({
    type: DataType.STRING,
    allowNull: true
  })
  edition: string;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false
  })
  isDamaged: boolean;

  @Column({
    type: DataType.STRING,
    allowNull: true
  })
  note: string;

  @Column({
    type: DataType.UUID,
    allowNull: true
  })
  user_id: string;
}
