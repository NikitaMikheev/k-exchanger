export class AddNewApplicationToListingDto {
  type: 'selling' | 'exchange';
  price?: number;
  comment: string;
  item_id: string;
}
