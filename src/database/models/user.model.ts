import { Table, Column, Model, DataType, BeforeCreate, AfterCreate } from 'sequelize-typescript';
import * as bcrypt from 'bcrypt';
import { generateToken } from 'src/tool/generateToken';
import { v4 } from 'uuid';

@Table
export class UserModel extends Model {
  @Column({
    type: DataType.UUID,
    allowNull: false,
    primaryKey: true,
    unique: true,
    defaultValue: () => v4()
  })
  id: string;

  @Column({
    type: DataType.STRING,
    allowNull: false
  })
  username: string;

  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false
  })
  email: string;

  @Column({
    type: DataType.STRING,
    allowNull: false
  })
  password: string;

  @Column({
    type: DataType.ENUM('admin', 'client'),
    allowNull: false,
    defaultValue: 'client'
  })
  role: 'admin' | 'client';

  @Column({
    type: DataType.DOUBLE,
    allowNull: false,
    defaultValue: 0
  })
  balance: number;

  @Column({
    type: DataType.STRING,
    allowNull: true
  })
  refreshToken: string;

  @BeforeCreate
  static async hashPassword(user: UserModel) {
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
  }

  @AfterCreate
  static async generateRefreshToken(user: UserModel) {
    user.refreshToken = generateToken(user, Number(process.env.REFRESH_TOKEN_LIFE_TIME) || 30 * 24 * 60 * 60 * 1000);
    await user.save();
  }
}
