export enum CardType {
  PREORDER = 'preorder',
  ALBUM = 'album',
  WINNER = 'winner',
  FANMEETINGS = 'fanmeetings',
  MDs = 'MDs',
  VIDEOCALL = 'videocall',
  FANSIGN = 'fansign',
  LUCKYDRAW = 'luckydraw',
  SEASON_GREETINGS = 'season_greetings',
  TRADING = 'trading',
  MISC = 'misc',
  UNKNOWN = 'unknown'
}

export enum AlbumType {
  PREORDER = 'preorder'
}
