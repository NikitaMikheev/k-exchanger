import { Request } from 'express';
import { UserModel } from 'src/database/models/user.model';

export interface UserRequest extends Request {
  user: UserModel;
}
